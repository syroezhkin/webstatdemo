package com.syroezhkin.webstatdemo.repository;

import com.syroezhkin.webstatdemo.domain.Visit;
import com.syroezhkin.webstatdemo.dto.VisitsReportDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by syroezhkin on 14/03/2018.
 */
@Repository
public interface VisitRepository extends JpaRepository<Visit, Long> {

    @Query(value = "select new com.syroezhkin.webstatdemo.dto.VisitsReportDto(count(v), count(DISTINCT v.userId)) " +
                   "from Visit v where v.createdDate >= :startDate and v.createdDate <= :endDate")
    List<VisitsReportDto> getTotalAndUniqueReport(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query(value = "select count(*) as pers from (" +
                   "select user_id, count(distinct page_id) hits from visit " +
                   "where created_date >= :startDate and created_date <= :endDate " +
                   "group by user_id having count(distinct page_id) >= :regularCriteria) a",
            nativeQuery = true)
    Long getRegularVisitors(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                            @Param("regularCriteria") Integer regularCriteria);
}
