package com.syroezhkin.webstatdemo.controller;

import com.syroezhkin.webstatdemo.domain.Visit;
import com.syroezhkin.webstatdemo.dto.VisitDto;
import com.syroezhkin.webstatdemo.dto.VisitsReportDto;
import com.syroezhkin.webstatdemo.service.CollectService;
import com.syroezhkin.webstatdemo.service.ReportService;
import com.syroezhkin.webstatdemo.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by syroezhkin on 14/03/2018.
 */
@RestController
@RequestMapping("/api")
public class WebStatController {

    @Autowired
    private ReportService reportService;

    @Autowired
    @Qualifier("collectServiceAsyncImpl")
    private CollectService collectService;

    @RequestMapping(value = "/visits", method = RequestMethod.GET)
    public List<Visit> getAllData() {
        return reportService.getAllVisits();
    }

    @RequestMapping(value = "/visits", method = RequestMethod.POST)
    public VisitsReportDto addVisit(@Valid @RequestBody VisitDto visitDto) {
        collectService.addVisit(visitDto);
        return reportService.getReport(CommonUtils.startOfDay(new Date()), CommonUtils.endOfDay(new Date()));
    }

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    public VisitsReportDto getReport(@RequestParam Long dateFrom, @RequestParam Long dateTo) {
        return reportService.getFullReport(new Date(dateFrom), new Date(dateTo));
    }

    @RequestMapping(value = "/report", method = RequestMethod.GET, params = "dateFormat=string")
    public VisitsReportDto getReport(@RequestParam String dateFrom, @RequestParam String dateTo) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = CommonUtils.startOfDay(sdf.parse(dateFrom));
        Date endDate = CommonUtils.endOfDay(sdf.parse(dateTo));
        return getReport(startDate.getTime(), endDate.getTime());
    }

}
