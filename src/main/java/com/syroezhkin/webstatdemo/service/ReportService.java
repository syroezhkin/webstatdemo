package com.syroezhkin.webstatdemo.service;

import com.syroezhkin.webstatdemo.domain.Visit;
import com.syroezhkin.webstatdemo.dto.VisitsReportDto;
import com.syroezhkin.webstatdemo.repository.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by syroezhkin on 15/03/2018.
 */
@Service
public class ReportService {

    /**
     * Amount of unique pages viewed for the period what is needed to be a regular visitor
     */
    private static final Integer REGULAR_VISITORS_CRITERIA = 10;

    @Autowired
    private VisitRepository visitRepository;

    public VisitsReportDto getReport(Date startDate, Date endDate) {
        return visitRepository.getTotalAndUniqueReport(startDate, endDate).stream().findFirst().orElse(null);
    }

    public List<Visit> getAllVisits() {
        return visitRepository.findAll();
    }

    public VisitsReportDto getFullReport(Date startDate, Date endDate) {
        VisitsReportDto report = getReport(startDate, endDate);
        report.setRegular(visitRepository.getRegularVisitors(startDate, endDate, REGULAR_VISITORS_CRITERIA));
        return report;
    }
}
