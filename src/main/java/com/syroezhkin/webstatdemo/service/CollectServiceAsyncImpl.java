package com.syroezhkin.webstatdemo.service;

import com.syroezhkin.webstatdemo.dto.VisitDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by syroezhkin on 15/03/2018.
 */
@Service
public class CollectServiceAsyncImpl implements CollectService {
    private static final Logger log = LoggerFactory.getLogger(CollectServiceAsyncImpl.class);

    @Autowired
    private QueueProcessor queueProcessor;

    @Override
    public void addVisit(VisitDto visitDto) {
        log.debug("Enter async process for storing {}", visitDto);
        queueProcessor.add(visitDto);
        log.debug("Exit async process for storing {}", visitDto);
    }


}
