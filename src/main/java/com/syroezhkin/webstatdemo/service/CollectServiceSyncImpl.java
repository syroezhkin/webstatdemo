package com.syroezhkin.webstatdemo.service;

import com.syroezhkin.webstatdemo.dto.VisitDto;
import com.syroezhkin.webstatdemo.mapper.VisitMapper;
import com.syroezhkin.webstatdemo.repository.VisitRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by syroezhkin on 15/03/2018.
 */
@Service
public class CollectServiceSyncImpl implements CollectService {
    private static final Logger log = LoggerFactory.getLogger(CollectServiceSyncImpl.class);

    @Autowired
    private VisitRepository visitRepository;

    @Override
    public void addVisit(VisitDto visitDto) {
        log.debug("Storing {} in database", visitDto);
        visitRepository.save(VisitMapper.map(visitDto));
    }
}
