package com.syroezhkin.webstatdemo.service;

import com.syroezhkin.webstatdemo.dto.VisitDto;

/**
 * Created by syroezhkin on 15/03/2018.
 */
public interface CollectService {

    void addVisit(VisitDto visitDto);

}
