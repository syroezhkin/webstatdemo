package com.syroezhkin.webstatdemo.service;

import com.syroezhkin.webstatdemo.dto.VisitDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by syroezhkin on 15/03/2018.
 */
@Service
public class QueueProcessor {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("collectServiceSyncImpl")
    private CollectService collectService;

    private final Queue<VisitDto> queue = new ConcurrentLinkedQueue<>();
    private Thread thread;
    private final AtomicBoolean running = new AtomicBoolean(false);

    public void add(VisitDto visitDto) {
        queue.add(visitDto);
    }

    @PostConstruct
    public void start() throws Exception {
        if (this.thread == null) {
            log.info("Run thread for QueueProcessor");
            this.thread = new Thread(() -> {
                running.set(true);
                while (running.get()) {
                    synchronized (queue) {
                        if (!queue.isEmpty()) {
                            log.debug("New queue item detected. Imitation of long processing");
                            try {
                                TimeUnit.SECONDS.sleep(3);
                            } catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                            }
                            Optional.ofNullable(queue.poll()).ifPresent(collectService::addVisit);
                        }
                    }
                }
                log.info("Exit thread for QueueProcessor");
            });
            thread.start();
        }
    }

    @PreDestroy
    public void stop() {
        if (thread != null) {
            log.info("Stopping QueueProcessor thread");
            running.set(false);
        }
    }
}
