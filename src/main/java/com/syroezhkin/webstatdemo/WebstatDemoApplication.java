package com.syroezhkin.webstatdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Created by syroezhkin on 14/03/2018.
 */
@SpringBootApplication
@EnableJpaAuditing
public class WebstatDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebstatDemoApplication.class, args);
    }

}
