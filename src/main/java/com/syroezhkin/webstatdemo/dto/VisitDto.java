package com.syroezhkin.webstatdemo.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by syroezhkin on 15/03/2018.
 */
public class VisitDto {

    @NotNull
    private Long pageId;

    @NotNull
    private Long userId;

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "VisitDto{" +
                "pageId=" + pageId +
                ", userId=" + userId +
                '}';
    }
}
