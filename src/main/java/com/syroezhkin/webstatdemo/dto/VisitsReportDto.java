package com.syroezhkin.webstatdemo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Created by syroezhkin on 15/03/2018.
 */
@JsonInclude(Include.NON_NULL)
public class VisitsReportDto {

    private Long total;
    private Long unique;
    private Long regular;

    public VisitsReportDto(Long total, Long unique) {
        this.total = total;
        this.unique = unique;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getUnique() {
        return unique;
    }

    public void setUnique(Long unique) {
        this.unique = unique;
    }

    public Long getRegular() {
        return regular;
    }

    public void setRegular(Long regular) {
        this.regular = regular;
    }
}
