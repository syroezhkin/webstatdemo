package com.syroezhkin.webstatdemo.mapper;

import com.syroezhkin.webstatdemo.domain.Visit;
import com.syroezhkin.webstatdemo.dto.VisitDto;

/**
 * Created by syroezhkin on 15/03/2018.
 */
public class VisitMapper {

    public static Visit map(VisitDto dto) {
        Visit entity = new Visit();
        entity.setPageId(dto.getPageId());
        entity.setUserId(dto.getUserId());
        return entity;
    }
}
