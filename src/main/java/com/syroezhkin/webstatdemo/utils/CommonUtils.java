package com.syroezhkin.webstatdemo.utils;

import org.apache.commons.lang3.time.DateUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * Created by syroezhkin on 15/03/2018.
 */
public class CommonUtils {

    public static Date startOfDay(Date date) {
        return Optional.ofNullable(date)
                .map(_d -> DateUtils.truncate(_d, Calendar.DATE))
                .orElse(null);
    }

    public static Date endOfDay(Date date) {
        return Optional.ofNullable(date)
                .map(_d -> DateUtils.addMilliseconds(DateUtils.ceiling(_d, Calendar.DATE), -1))
                .orElse(null);
    }

}
