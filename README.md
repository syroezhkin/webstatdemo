# README #

This is a demo application which implements a REST-API service for collecting webstat data and getting report.


## Тестовое задание Java ##

Требуется реализовать REST-сервис по сбору статистики посещаемости WEB-сайта.
Сервис должен поддерживать два метода:

1. Создание события посещения сайта пользователем. Параметры:
    
    1. Идентификатор пользователя
    2. Идентификатор страницы сайта

    Ответ должен содержать (в формате JSON):
    
    1. Общее количество посещений за текущие сутки
    2. Количество уникальных пользователей за текущие сутки

2. Получение статистики посещения за произвольный период. Параметр запроса:
    
    1. период учёта

    Ответ должен содержать (в формате JSON):

    1. Общее количество посещений за указанный период
    2. Количество уникальных пользователей за указанный период
    3. Количество постоянных пользователей за указанный период
(пользователей, которые за период просмотрели не менее 10 различных
страниц).

В качестве хранилища информации можно использовать любую СУБД.
Авторизации в сервисе не требуется.

Желательно, чтобы сохранение события в постоянное
хранилище в первом методе выполнялось асинхронно для оптимизации под высокую
нагрузку.


## How to run ##

1. Package with maven

    `mvn package` 

2. Run jar file

    `java -jar target/webstat-demo-1.0.0-SNAPSHOT.jar`
    
## How to use ##

[Postman]("https://www.getpostman.com/) collection is included into repository. 
Just import WebStatDemo.postman_collection.json file.

#### POST /api/visits ####

Posts new visit and returns total today visits and unique users.

**Required header:**
`Content-Type: application/json`

**Body request:**
`{
  	"pageId": 1,
 	"userId": 15
 }`
 
**Response:**
`{
    "total": 3,
    "unique": 1
}`


#### GET /api/visits ####

Return list of all visits. Just for checking what database contains.


#### GET /api/report ####

Returns report for the period: total visits, unique users and regular users, who've watched more than 10 unique pages 
for the period

**Parameters**

Two types of parameters set (I would use it for frontend developers):

`dateFrom` – start timestamp (milliseconds since Unix epoch)
`dateTo` – end timestamp (milliseconds since Unix epoch)

another set (just for convenient testing):

`dateFrom` – start date in string format yyyy-MM-dd. 
Will be calculating beginning of date for this value
`dateTo` – end date in string format yyyy-MM-dd.
Will be calculating ending of date for this value
`dateFormat` – `string`

**Response:** 

`{
   "total": 3,
   "unique": 1,
   "regular": 0
}`


## How to check ##

On first run new database will be created with demo data. 
Demo data is loading from /src/main/resources/data-derby.sql file

**Report for 2018-03-15**

Only user 10 is regular. User 11 has only 9 unique pages although it has 12 visits for this day.

`http://localhost:8080/api/report?dateFrom=2018-03-15&dateTo=2018-03-15&dateFormat=string`

`{
     "total": 40,
     "unique": 5,
     "regular": 1
 }`
 
**Report for 2018-03-16**

Only user 11 has 3 visits on 3 different pages for this day

`http://localhost:8080/api/report?dateFrom=2018-03-16&dateTo=2018-03-16&dateFormat=string`

`{
  "total": 3,
  "unique": 1,
  "regular": 0
}`

**Report for two days**

Now user 11 has more than 10 unique pages for period. It is regular user too.

`http://localhost:8080/api/report?dateFrom=2018-03-15&dateTo=2018-03-16&dateFormat=string`

`{
   "total": 43,
   "unique": 5,
   "regular": 2
}`